#!/bin/sh
source ./docker-env.sh

docker run -d -it --rm -p ${T8_PORT}:8080 -h tomkatz --name ${NAME_CNT} ${NAME_IMG}

curl http://127.0.0.1:${T8_PORT}/webapps/hello/