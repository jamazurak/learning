#!/bin/sh
export NAME_IMG="tomcat:8"
export NAME_CNT="Tomcat8"
export T8_PORT=8080

case $1 in
	build)
    	echo "Build a docker image"
		docker build -t ${NAME_IMG} .
        break
        ;;
    run)
    	echo "Run a docker image"
		docker run -d -it --rm --mount type=bind,source=/Users/ymazurak/docker/tomcat-vol,target=/katz,readonly=false -p ${T8_PORT}:8080 -h tomkatz --name ${NAME_CNT} ${NAME_IMG}
        break
        ;;
    exec)
    	echo "Execute command in a running docker image"
        echo "Host view:"
        ls -l /Users/ymazurak/docker/tomcat-vol
		docker exec ${NAME_CNT} touch /katz/tom.txt
        echo "Container view:"
		docker exec ${NAME_CNT} ls -l /katz
        echo "Host view again:"
        ls -l /Users/ymazurak/docker/tomcat-vol
        break
        ;;
    test)
    	echo "Do test(s)"
		curl http://127.0.0.1:${T8_PORT}/webapps/hello/
        break
        ;;
    stop)
    	echo "Stop a running docker image"
		docker kill ${NAME_CNT}
        break
        ;;
    remove)
    	echo "Remove a docker image"
		docker image rm ${NAME_IMG}
        break
        ;;
    *)
    	echo "\"$1\"? Invalid choice. Exiting"
        break
        ;;
esac