#!/bin/sh
source ./dodocker.sh build
source ./dodocker.sh run
read -p "Pause 7 seconds for Tomcat startup... " -t 7
echo "...continue"
source ./dodocker.sh exec
source ./dodocker.sh test
source ./dodocker.sh stop
source ./dodocker.sh remove