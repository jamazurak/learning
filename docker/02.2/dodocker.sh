#!/bin/sh
export NAME_IMG="bubuntu"
export NAME_CNT="BUbuntu"

case $1 in
	build)
    	echo "Build a docker image"
		docker build -t ${NAME_IMG} .
        break
        ;;
    run)
    	echo "Run a docker image"
		docker run -d -it --rm --name ${NAME_CNT} ${NAME_IMG}
        break
        ;;
    exec)
    	echo "Execute command in a running docker image"
        echo "Container view:"
		docker exec ${NAME_CNT} whoami
        break
        ;;
    test)
    	echo "Do test(s)"
		docker exec ${NAME_CNT} ls /home
        break
        ;;
    stop)
    	echo "Stop a running docker image"
		docker kill ${NAME_CNT}
        break
        ;;
    remove)
    	echo "Remove a docker image"
		docker image rm ${NAME_IMG}
        break
        ;;
    *)
    	echo "\"$1\"? Invalid choice. Exiting"
        break
        ;;
esac