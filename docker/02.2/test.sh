#!/bin/sh
source ./dodocker.sh build
source ./dodocker.sh run
read -p "Pause 3 seconds for container startup... " -t 3
echo "...continue"
source ./dodocker.sh exec
source ./dodocker.sh test
source ./dodocker.sh stop
source ./dodocker.sh remove