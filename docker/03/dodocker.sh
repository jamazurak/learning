#!/bin/sh
# Image name
IMG_SERVER="d03tomcat"
IMG_CLIENT="d03ubuntu"
# Startup pause time
SPT_SERVER=7
SPT_CLIENT=3
# Network name
NET_NAME="docker03"
# Web server port
WS_PORT=8080

echo "Create network ${NET_NAME}"
docker network create ${NET_NAME} --subnet 192.168.192.0/24 --aux-address="${IMG_SERVER}=192.168.192.111" --aux-address="${IMG_SERVER}=192.168.192.222"
docker network ls --filter name=${NET_NAME} --no-trunc

echo "Build a docker images"
docker build -t "${IMG_SERVER}" --build-arg WS_PORT=${WS_PORT} server
docker build -t "${IMG_CLIENT}" client

echo "Run docker containers"
docker run -d --rm --network ${NET_NAME} --net-alias ${IMG_SERVER} --name ${IMG_SERVER} ${IMG_SERVER}
read -p "Pause ${SPT_SERVER} seconds for startup ${IMG_SERVER}... " -t ${SPT_SERVER}
echo "done."
docker run -dit --rm --network ${NET_NAME} --net-alias ${IMG_CLIENT} --name ${IMG_CLIENT} ${IMG_CLIENT}
read -p "Pause ${SPT_CLIENT} seconds for startup ${IMG_CLIENT}... " -t ${SPT_CLIENT}
echo "done."

echo "List running containers"
docker ps

echo "Do network transfer test"
docker exec ${IMG_CLIENT} curl --get http://${IMG_SERVER}:${WS_PORT}/webapps/hello/

echo "Stop a running docker images"
docker kill ${IMG_CLIENT} ${IMG_SERVER}

echo "Remove a docker images"
docker image rm ${IMG_CLIENT} ${IMG_SERVER}

echo "Remove network ${NET_NAME}"
docker network rm ${NET_NAME}
