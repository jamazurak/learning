#!/bin/sh
export IP_CLIENT="192.168.192.111"
export IP_SERVER="192.168.192.222"
export IP_PORT="8080"

echo "*** Set working directory"
cd ~/learning/docker/04
pwd

echo "*** List images"
docker images -a

echo "*** Build images"
docker-compose build

echo "*** List images"
docker images -a

echo "*** Start web server"
docker-compose up -d webserver
read -p "Waiting for startup the web server... " -t 10
echo "done."

echo "*** List running containers"
docker ps

# docker-compose exec webserver bash

echo "*** Do network transfer test"
echo docker-compose run client curl --get http://${IP_SERVER}:${IP_PORT}/hello/
docker-compose run client curl --get http://${IP_SERVER}:${IP_PORT}/hello/

echo "*** Stop and remove running docker images"
docker-compose rm --force --stop -v

echo "*** Shutdown orphan(s)"
docker-compose down --remove-orphans
